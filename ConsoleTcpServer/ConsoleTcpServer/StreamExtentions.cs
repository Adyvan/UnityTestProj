﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;

namespace ConsoleTcpServer
{

  public static class StreamExtentions
  {
    public static byte[] ReadAllByte(this NetworkStream stream)
    {
      var bytes = new List<byte>();
      if (stream.DataAvailable)
      {
        int blockSize = 1024;
        byte[] buffers = new byte[blockSize];
        int offset = 0;
        int incrementOffset = 0;
        while ((incrementOffset = stream.Read(buffers, offset, blockSize)) > 0)
        {
          bytes.AddRange(buffers.Take(incrementOffset));
          offset += incrementOffset;
          if (incrementOffset < blockSize)
            break;
        }
      }
      return bytes.ToArray();
    }
  }
}