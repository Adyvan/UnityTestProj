﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ConsoleTcpServer
{
  public class MyTcpServer
  {
    bool IsWork;
    TcpListener Listener;

    byte[] currentState;

    byte[] CurrentState
    {
      get { return currentState; }
      set
      {
        currentState = value;
        Console.WriteLine($"DataUpdate:  {Encoding.ASCII.GetString(currentState)}");
        UpdateNewState();
      }
    }

    List<TcpClient> Clients = new List<TcpClient>();

    // Use this for initialization
    public void Start()
    {
      Listener = new TcpListener(System.Net.IPAddress.Any, 48656);
      Listener.Start();
      IsWork = true;

      System.Threading.ThreadPool.QueueUserWorkItem(x => Listening());
      UpdatingListeners();
    }

    void Listening()
    {
      string localIP;
      using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
      {
        socket.Connect("8.8.8.8", 65530);
        IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
        localIP = endPoint.Address.ToString();
      }
      Console.WriteLine("Addres :" + localIP); //127.0.0.1
      while (IsWork)
      {
        try
        {
          var cliernt = Listener.AcceptTcpClient();
          Clients.Add(cliernt);
          Console.WriteLine("NewClient!!!");
          Console.WriteLine($"Clients Count {Clients.Count}");
        }
        catch (Exception e)
        {
          Console.WriteLine("Error: " + e);
        }
      }
    }

    void UpdatingListeners()
    {
      while (IsWork)
      {
        System.Threading.Thread.Sleep(0);
        var clients = Clients.ToArray();
        foreach (var client in clients)
        {
          try
          {
            if (client.Connected && client.Available > 0)
            {
              try
              {
                var newState = client.GetStream().ReadAllByte();
                if (newState.Length > 0)
                {
                  CurrentState = newState;
                }
              }
              catch (Exception e)
              {
                Console.WriteLine("Error: " + e);
              }
            }
            else if (!client.Connected)
            {
              Console.WriteLine("Remove client!");
              client.Close();
              Clients.Remove(client);
              Console.WriteLine($"Clients Count {Clients.Count}");
            }
          }
          catch { }
        }
      }
    }

    void UpdateNewState()
    {
      var clients = Clients.ToArray();
      foreach (var client in clients)
      {
        if (client.Connected)
        {
          try
          {
            client.GetStream().Write(CurrentState, 0, CurrentState.Length);
          }
          catch (Exception ex)
          {
            Console.WriteLine("Error: " + ex);
          }
        }
        else
        {
          Console.WriteLine("Remove client!");
          client.Close();
          Clients.Remove(client);
          Console.WriteLine($"Clients Count {Clients.Count}");
        }
      }
    }

    void OnApplicationQuit()
    {
      CloseServer();
    }

    void CloseServer()
    {
      IsWork = false;

      var clients = Clients.ToArray();
      foreach (var client in clients)
      {
        client.Close();
      }
      Clients.Clear();

      if (Listener != null)
      {
        Listener.Stop();

      }
      Listener = null;
    }

    ~MyTcpServer()
    {
      CloseServer();
    }
  }
}
