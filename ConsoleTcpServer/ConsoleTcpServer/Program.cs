﻿using System;

namespace ConsoleTcpServer
{
  class MainClass
  {
    public static void Main(string[] args)
    {
      try
      {
        MyTcpServer va = new MyTcpServer();
        va.Start();
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex);
      }
      Console.ReadLine();
    }
  }
}
