﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {

  public float loopduration;
  float time;
  float disposeTime = 1;
  Renderer rend;
	// Use this for initialization
	void Start () {
    rend = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {

    Destroy (gameObject, 3);

    time += Time.deltaTime * 2.5f;
    disposeTime -= Time.deltaTime;
    float r = Mathf.Sin((Time.time / loopduration) * (2 * Mathf.PI)) * 0.5f + 0.25f;
    float g = Mathf.Sin((Time.time / loopduration + 0.33333333f) * 2 * Mathf.PI) * 0.5f + 0.25f;
    float b = Mathf.Sin((Time.time / loopduration + 0.66666667f) * 2 * Mathf.PI) * 0.5f + 0.25f;
    float correction = 1 / (r + g + b);
    r *= correction;
    g *= correction;
    b *= correction;
    rend.material.SetVector("_ChannelFactor", new Vector4(r,g,b,0));

    rend.material.SetVector("_Range", new Vector4(time,0,0,0));
    rend.material.SetFloat("_ClipRange", disposeTime);
	}
}
