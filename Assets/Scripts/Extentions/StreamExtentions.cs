﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;
using System.Net.Sockets;
using System.Linq;

namespace StreamExtentions
{
  public static class StreamExtentions
  {
    public static byte[] ReadAllByte (this NetworkStream stream)
    {
      var bytes = new List<byte> ();
      if (stream.DataAvailable) {
        int blockSize = 1024;
        byte[] buffers = new byte[blockSize];
        int offset = 0;
        int incrementOffset = 0;
        while ((incrementOffset = stream.Read (buffers, offset, blockSize)) > 0) {
          bytes.AddRange (buffers.Take (incrementOffset));
          offset += incrementOffset;
          if (incrementOffset < blockSize)
            break;
        }
      }
      return bytes.ToArray ();
    }


    public static T ReadObject<T> (this NetworkStream stream) where T : class
    {
      T state = null;
      var bytes = stream.ReadAllByte ();

      if (bytes.Length > 0) {
        var data = Encoding.ASCII.GetString (bytes);
        try {
          state = JsonConvert.DeserializeObject<T> (data);
        } catch (Exception ex) {
          Debug.Log ("Error " + data + "\n" + ex);
        }
      }
      return state;
    }

    public static void WriteState (this NetworkStream stream, GameState state)
    {
      var stateString = JsonConvert.SerializeObject (state);

      var data = Encoding.ASCII.GetBytes (stateString);

      stream.Write (data, 0, data.Length);
    }
  }
}