﻿using Newtonsoft.Json;

public class GameState
{
  [JsonProperty ("IsLight")]
  public bool IsLight { get; set; }

  [JsonProperty ("IsExplosion")]
  public bool IsExplosion { get; set; }

  public GameState Copy ()
  {
    return new GameState {
      IsLight = IsLight,
      IsExplosion = IsExplosion,
    };
  }
}
