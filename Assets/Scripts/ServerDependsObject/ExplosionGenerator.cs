﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class ExplosionGenerator : GettingDataClient
{
  public Transform Explosion;

  public override void Start()
  {
    base.Start ();
    GettingState.Where (x => x.IsExplosion).ObserveOnMainThread().Subscribe (StatusChanged);
  }
 
  public void StatusChanged(GameState status) {
    var pos = new Vector3(Random.Range(0, 100)/20f - 2.5f,1,(Random.Range(0, 100)/20f - 4f));
    Instantiate (Explosion, transform.position + pos, transform.rotation);
    CurrentState.IsExplosion = false;
  }
}
