﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

public class ExplosionInvoker : SendDataClient
{
  Button button;

  public override void Start ()
  {
    base.Start ();
    button = gameObject.GetComponent<Button> ();
    button.onClick.AsObservable().Subscribe (ExplosionFire);
  }
  
  public void ExplosionFire(Unit button)
  {
    InvokeExplosion ();
  }
}
