﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class LightScript : GettingDataClient
{
  Light _light;

  public override void Start ()
  {
    base.Start ();
    _light = gameObject.GetComponent<Light> ();
    GettingState.ObserveOnMainThread ().Where(x => _light.enabled != x.IsLight).Subscribe (StatusChanged);
  }

  public void StatusChanged(GameState status) 
  {
    _light.enabled = status.IsLight;
  }
}
