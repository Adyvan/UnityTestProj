﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightSwitcher : SendDataClient
{
  Toggle swicher;

  public override void Start ()
  {
    base.Start ();
    swicher = gameObject.GetComponent<Toggle> ();
    swicher.isOn = CurrentState.IsLight;
    swicher.onValueChanged.AddListener (UpdateValue);
  }

  void UpdateValue (bool val)
  {
    SwichLight (val);
  }
}