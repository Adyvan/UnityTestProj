﻿using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;
using System;
using StreamExtentions;
using System.Net;
using System.Text;

public class ServerScript : MonoBehaviour
{
  bool IsWork;
  TcpListener Listener;

  byte[] currentState;

  byte[] CurrentState
  {
    get { return currentState; }
    set
    {
      currentState = value;
      Console.WriteLine("DataUpdate:" + Encoding.ASCII.GetString(currentState));
      UpdateNewState();
    }
  }

  List<TcpClient> Clients;

  void Start()
  {
    Clients = new List<TcpClient>();
    Listener = new TcpListener(System.Net.IPAddress.Any, 48656);
    Listener.Start();
    IsWork = true;

    System.Threading.ThreadPool.QueueUserWorkItem(x => Listening());
    System.Threading.ThreadPool.QueueUserWorkItem(x => UpdatingListeners());
  }

  void Listening()
  {
    while (IsWork)
    {
      try
      {
        var cliernt = Listener.AcceptTcpClient();
        Clients.Add(cliernt);
        Console.WriteLine("NewClient!!!");
        Console.WriteLine("Clients Count " + Clients.Count);
      }
      catch (Exception e)
      {
        Console.WriteLine("Error: " + e);
      }
    }
  }

  void UpdatingListeners()
  {
    while (IsWork)
    {
      System.Threading.Thread.Sleep(0);
      var clients = Clients.ToArray();
      foreach (var client in clients)
      {
        try
        {
          if (client.Connected && client.Available > 0)
          {
            try
            {
              var newState = client.GetStream().ReadAllByte();
              if (newState.Length > 0)
              {
                CurrentState = newState;
              }
            }
            catch (Exception e)
            {
              Console.WriteLine("Error: " + e);
            }
          }
          else if (!client.Connected)
          {
            Console.WriteLine("Remove client!");
            client.Close();
            Clients.Remove(client);
            Console.WriteLine("Clients Count " + Clients.Count);
          }
        } catch {}
      }
    }
  }

  void UpdateNewState()
  {
    var clients = Clients.ToArray();
    foreach (var client in clients)
    {
      if (client.Connected)
      {
        try
        {
          client.GetStream().Write(CurrentState, 0, CurrentState.Length);
        }
        catch (Exception ex)
        {
          Console.WriteLine("Error: " + ex);
        }
      }
      else
      {
        Console.WriteLine("Remove client!");
        client.Close();
        Clients.Remove(client);
        Console.WriteLine("Clients Count " + Clients.Count);
      }
    }
  }

  void OnApplicationQuit()
  {
    CloseServer();
  }

  void CloseServer()
  {
    IsWork = false;

    if (Clients != null) {
      var clients = Clients.ToArray ();
      foreach (var client in clients) {
        client.Close ();
      }
      Clients.Clear ();
    }

    if (Listener != null)
    {
      Listener.Stop();

    }
    Listener = null;
  }

  ~ServerScript()
  {
    CloseServer();
  }
}