﻿using UnityEngine;
using System.Net.Sockets;
using System;
using System.Net;
using StreamExtentions;
using UniRx;

public class GettingDataClient : MonoBehaviour
{
  protected static ISubject<GameState> sendingState = new Subject<GameState> ();

  static ISubject<GameState> gettingState = new Subject<GameState> ();

  protected ISubject<GameState> GettingState { get { return gettingState; } }

  static GameState currentState;
  protected GameState CurrentState {
    get { return currentState; }
    set { 
      currentState = value;
      GettingState.OnNext (value);
    }
  }

  protected bool IsWork { get; private set; }

  protected static TcpClient client;

  public virtual void Start ()
  {
    ClientInitIfNeeded ();
  }

  void ClientInitIfNeeded ()
  {
    if (client == null) {
      CurrentState = new GameState ();
      client = new TcpClient ();
      System.Threading.ThreadPool.QueueUserWorkItem (x => TryConnect ());
      sendingState.Subscribe (SendState);
    }
  }

  void TryConnect ()
  {
    IPAddress ip = null;
    while (client != null && !IsWork) {
      try {
        if (ip == null) {
          ip = GetCurrentIP ();
        }
        client.Connect (ip, 48656);
        IsWork = true;
        Debug.Log ("ClientConnected");
      } catch (SocketException e) {
        Debug.Log (e);
        System.Threading.Thread.Sleep (200);
      } catch (NullReferenceException) {
        if (client == null)
          return;
      }
    }
    UpdatedStateFromServer ();
  }

  void UpdatedStateFromServer ()
  {
    while (IsWork) {
      var stream = client.GetStream ();
      if (stream.DataAvailable) {
        var newState = stream.ReadObject<GameState> ();
        if (newState != null) {
          CurrentState = newState;
        }
      }
    }
  }

  void SendState (GameState newState)
  {
    System.Threading.ThreadPool.QueueUserWorkItem (x => {
      try {
        client.GetStream ().WriteState (newState);
      } catch (Exception ex) {
        Debug.Log (ex);
      }
    }
    );
  }

  IPAddress GetCurrentIP ()
  {
    IPAddress localIP;
    using (Socket socket = new Socket (AddressFamily.InterNetwork, SocketType.Dgram, 0)) {
      socket.Connect ("8.8.8.8", 65530);
      IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
      localIP = endPoint.Address;
    }
    Debug.Log ("Work with" + localIP);
    return localIP; //127.0.0.1
  }

  public virtual void OnApplicationQuit ()
  {
    IsWork = false;
    CurrentState = null;
    if (client != null) {
      client.Close ();
    }
    client = null;
  }
}
