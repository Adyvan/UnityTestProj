﻿using System;
using UniRx;
using UnityEngine;
using StreamExtentions;

public class SendDataClient : GettingDataClient
{
  protected void InvokeExplosion ()
  {
    if (client.Connected) {
      var newState = CurrentState.Copy ();
      newState.IsExplosion = true;
      sendingState.OnNext (newState);
    }
  }

  protected void SwichLight (bool isOn)
  {
    if (client.Connected) {
      var newState = CurrentState.Copy ();
      newState.IsLight = isOn;
      sendingState.OnNext (newState);
    }
  }
}

